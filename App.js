import React from 'react';
import {
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';

const App = () => {
  return (
    <View style={styles.container1}>
      <View style={styles.container2}>
        <View style={styles.tittle}>
          <Text style={styles.text}>Digital Approval</Text>
        </View>
        <Image source={require('./logo.png')} style={styles.image} />
        <TextInput
          style={styles.input1}
          keyboardType="email"
          placeholder="Alamat email"></TextInput>
        <TextInput
          style={styles.input2}
          keyboardType="password"
          placeholder="Password"></TextInput>
        <Text style={styles.text2}>Reset Password</Text>
        <TouchableOpacity style={styles.button}>
          <Text
            style={{
              color: '#FFFFFF',
              textAlign: 'center',
              fontSize: 16,
              fontWeight: 'bold',
            }}>
            LOGIN
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container1: {
    backgroundColor: '#F7F7F7',
    flex: 1,
    justifyContent: 'center',
  },

  container2: {
    backgroundColor: '#FFFFFF',
    paddingHorizontal: 20,
    marginHorizontal: 20,
    alignItems: 'center',
    height: 430,
    borderRadius: 8,
  },

  tittle: {
    backgroundColor: '#002558',
    width: 170,
    height: 36,
    justifyContent: 'center',
    borderRadius: 20,
    marginTop: -18,
  },

  text: {
    color: 'white',
    textAlign: 'center',
    fontSize: 16,
    fontWeight: 'bold',
    fontFamily: 'Roboto',
  },

  image: {
    marginTop: 45,
    marginLeft: 97,
    marginRight: 96,
  },

  input1: {
    height: 48,
    width: 288,
    borderWidth: 1,
    marginTop: 38,
    borderRadius: 5,
    borderColor: '#002558',
  },

  input2: {
    height: 48,
    width: 288,
    borderWidth: 1,
    marginTop: 24,
    borderRadius: 5,
    borderColor: '#002558',
  },

  text2: {
    marginTop: 16,
    color: '#287AE5',
    marginLeft: 201,
    fontStyle: 'italic',
    fontFamily: 'Roboto',
  },

  button: {
    justifyContent: 'center',
    backgroundColor: '#287AE5',
    width: 288,
    height: 48,
    borderRadius: 5,
    marginTop: 24,
  },
});

export default App;
